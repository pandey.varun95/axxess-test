package com.example.axeesstest;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivityViewModel extends ViewModel implements IVolleyResponseListener {
    public static final String TAG = "MainActivityViewModel";
    private RequestQueue queue;
    private ArrayList<MyDataModel> myDataModelArrayList = new ArrayList<>();
    private MutableLiveData<ArrayList<MyDataModel>> mutableLiveDataList;
    private String url = "https://api.imgur.com/3/gallery/search/1?q=vanilla";


    public MutableLiveData<ArrayList<MyDataModel>> getMutableLiveDataList() {
        if (mutableLiveDataList == null) {
            mutableLiveDataList = new MutableLiveData<>();
        }
        return mutableLiveDataList;
    }

    public void hitApi(Context context) {
        Utils.startStringRequest(context, this,url);
    }

    private void parseMyJsonData(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                MyDataModel myDataModel = new MyDataModel();
                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                myDataModel.setId(jsonObject2.optString("id"));
                myDataModel.setTitle(jsonObject2.optString("title"));
                String link = jsonObject2.optString("link");
                String imgLink = null;
                if (jsonObject2.has("images")) {
                    JSONArray jsonArray2 = jsonObject2.getJSONArray("images");
                    JSONObject jsonObject3 = jsonArray2.getJSONObject(0);
                    imgLink = jsonObject3.optString("link");
                } else {
                    Log.d("TAG", "parseMyJsonData: ");
                }

                if (!link.contains(".jpg")) {
                    myDataModel.setLink(imgLink);
                } else {
                    myDataModel.setLink(jsonObject2.optString("link"));
                }
                myDataModelArrayList.add(myDataModel);
            }

            mutableLiveDataList.setValue(myDataModelArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {
        parseMyJsonData(response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "onErrorResponse: " + error.getMessage());
    }
}

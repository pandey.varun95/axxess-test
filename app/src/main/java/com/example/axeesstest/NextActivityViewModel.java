package com.example.axeesstest;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;


public class NextActivityViewModel extends ViewModel {

 public static final String PREFS_NAME ="Comment";
MutableLiveData<ArrayList<String>>mutableLiveDataList;
public MutableLiveData<ArrayList<String>> getMutableLiveDataList(){
    if(mutableLiveDataList==null) {
        mutableLiveDataList = new MutableLiveData<>();
    }
    return mutableLiveDataList;
}


    public void putDataToDB(ArrayList<String> stringArrayList, String id, Context context) {

        Gson gson = new Gson();
        String json = gson.toJson(stringArrayList);

        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(id,json);
        editor.apply();
        getDataFromDB(context,id);
    }

    public void getDataFromDB(Context context, String id) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        ArrayList<String>arrayList=new ArrayList<>();
        String json=prefs.getString(id,"");
        try {
            JSONArray jsonArray=new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                String str=jsonArray.getString(i);
                arrayList.add(str);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mutableLiveDataList.setValue(arrayList);
    }

}

package com.example.axeesstest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<MyDataModel> mDataList;
    public static final String TAG="Adapter";
    public void setData(Context context, ArrayList<MyDataModel> dataList) {
       this.mContext=context;
        this.mDataList=dataList;
        notifyDataSetChanged();
    }




    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.layout_adapter,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
       final MyDataModel myDataModel=mDataList.get(position);
        Picasso.with(mContext).load(myDataModel.getLink()).into(holder.imageView);
        holder.imageView.setOnClickListener(v -> {

            Intent intent = new Intent(mContext, NextActivity.class);
            intent.putExtra("id",myDataModel.getId());
            intent.putExtra("image",myDataModel.getLink());
            intent.putExtra("title",myDataModel.getTitle());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mParent;
        ImageView imageView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mParent=itemView.findViewById(R.id.parent);
            imageView=itemView.findViewById(R.id.imageView);
        }
    }
}

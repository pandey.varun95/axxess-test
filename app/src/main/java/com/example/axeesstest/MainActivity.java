package com.example.axeesstest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Adapter;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
     private MainActivityViewModel myViewModel;
     MyAdapter adapter;
     RecyclerView recyclerView;
     EditText editText;
     ArrayList<MyDataModel>dataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myViewModel= new ViewModelProvider(this).get(MainActivityViewModel.class);
        if(Utils.isInternetAvailable(this)){
            myViewModel.hitApi(this);
        }else{
            Toast.makeText(this,"Please Turn on Internet",Toast.LENGTH_LONG).show();
        }

        recyclerView=findViewById(R.id.recyclerView);
        editText=findViewById(R.id.searchView);
        adapter= new MyAdapter();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayoutManager);
        getData();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }
    void filter(String text){
        ArrayList<MyDataModel> tempList = new ArrayList();
        for(MyDataModel myDataModel: dataList){
            if(myDataModel.getTitle().toLowerCase().contains(text)||myDataModel.getTitle().contains(text)){
                tempList.add(myDataModel);
            }
        }
        adapter.setData(MainActivity.this,tempList);
    }

    private void getData() {
        myViewModel.getMutableLiveDataList().observe(this, myDataModelsList -> {
            if(myDataModelsList!=null){
                dataList=myDataModelsList;
                adapter.setData(MainActivity.this,myDataModelsList);
                recyclerView.setAdapter(adapter);
            }

        });
    }
}
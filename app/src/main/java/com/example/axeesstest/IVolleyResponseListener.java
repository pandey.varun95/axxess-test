package com.example.axeesstest;

import com.android.volley.VolleyError;

public interface IVolleyResponseListener {
    void onResponse(String response);

    void onErrorResponse(VolleyError error);

}

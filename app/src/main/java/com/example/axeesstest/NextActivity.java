package com.example.axeesstest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NextActivity extends AppCompatActivity {
    ImageView myImageView, backButton;
    TextView titleText;
    AppCompatEditText edtComment;
    Button btnSubmit;
    NextActivityViewModel nextActivityViewModel;
    String id;
    ArrayList<String> arrayList = new ArrayList<>();
    ListView listView;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);
        nextActivityViewModel = new ViewModelProvider(this).get(NextActivityViewModel.class);
        myImageView = findViewById(R.id.detail_image);
        backButton = findViewById(R.id.back_arrow);
        titleText = findViewById(R.id.title_text);
        edtComment = findViewById(R.id.edt_comment);
        btnSubmit = findViewById(R.id.btn_submit);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra("image");
        titleText.setText(intent.getStringExtra("title"));
        id = intent.getStringExtra("id");

        Picasso.with(this).load(imageUrl).into(myImageView);

        backButton.setOnClickListener(v -> finish());
        btnSubmit.setOnClickListener(v -> {

            String str = edtComment.getText().toString();
            if (!str.isEmpty() && str != null) {

                arrayList.add(str);
                nextActivityViewModel.putDataToDB(arrayList, id, NextActivity.this);
                edtComment.setText(" ");
            }
        });

        listView = findViewById(R.id.list_view);

        nextActivityViewModel.getMutableLiveDataList().observe(this, stringList -> {
           arrayList = stringList;
            adapter = new ArrayAdapter<String>(NextActivity.this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, arrayList);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        });

        nextActivityViewModel.getDataFromDB(this,id);
    }

}
